# Cinemakuy Api Backend
["https://cinemakuy.herokuapp.com"](https://cinemakuy.herokuapp.com)

## What is an API
API is the acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. Each time you use an app like Facebook, send an instant message, or check the weather on your phone, you’re using an API.

## Summary
Cinemakuy API is task from our lecture to make or build our own Apps, cinemakuy using API system same other API. There are many function in this API such as account managing , show movie schedule , show cinema schedule , order cinema seat and pay cinema.

This API is made by [Node.js](https://nodejs.org/en/), [npm express](https://www.npmjs.com/package/express) and [npm firebase](https://www.npmjs.com/package/firebase) for the core of this API.

## Usage
API is easy to use, you can search on the google "<your framework> get http request" or something like that
for example "ajax get http request" >> ["How Get Http Request From Ajax"](https://www.w3schools.com/jquery/jquery_ajax_get_post.asp)

## Testing
There are many way to test api, the simplest way is just browse the link, lets try this

#### Welcome [GET]
###### URL
```
https://cinemakuy.herokuapp.com
```
###### Response
```
{ 	
	"Welcome To This API"
}
```

#### Test Add [POST]
###### URL
```
https://cinemakuy.herokuapp.com/test/add
```
###### Request Body
```
{
    "v1": "<any data key>",
	"v2" : "<any data info>"
}
```
###### Response
```
{ 	
	respon : "<message>",
	isSuccess : <boolean is success>
}
```

#### Test Get [GET]
###### URL
```
https://cinemakuy.herokuapp.com/test/get
```
###### Request Body
```
{
    "v1": "<any data key>",
}
```
###### Response
```
{ 	
	"<value v2 of v1>"
}
```

#### Test Del [POST]
###### URL
```
https://cinemakuy.herokuapp.com/test/del
```
###### Request Body
```
{
    "v1": "<any data key>"
}
```
###### Response
```
{ 	
	respon : "<message>",
	isSuccess : <boolean is success>
}
```


but if you just browser, only one you can test `GET` method

so if you want to try all of this API functionality, use postman apps, you can download it below
["Download Postman"](https://www.getpostman.com/downloads/)


## Ready API
Ready HTTP request for this Api was listed below

### Account
This account API '/account' is focus on user account activity such as login, registration, update and logout. 

#### Check User Account [GET]
Get user account in firebase database using username and token for security. Token value should not an empty string.
###### URL
```
https://cinemakuy.herokuapp.com/account/get
```
###### Request Body
```
	{
	"username":"<username>",
	"token" : "<token>"
	}
```
###### Response
```
{
    "respon": "<Success/Failed>",
    "isSuccess": <True/False>,
	"name":"<user display name>",
	"phone":"<user phone number>",
	"email":"<user email address>",
	"credit":"<user total credit>"
	
}
```


#### Registration User Account [POST]
This API used to register new user account to database. First we will check if the user account is already exist or not. if not then the new account should be registered and saved at firebase database.

###### URL
```
https://cinemakuy.herokuapp.com/account/registration
```
###### Request Body
```
	{
	"name":"<Display Name>",
	"pass" : "<Password>",
	"email":"<User E-mail>",
	"phone":"<Phone Number>",
	"username":"<Username>"
	}
```

###### Response
```
{
    "respon": "User created successfully",
    "isSuccess": true
}
```

#### Login User Account [GET]
This API used to let the user login into their account if their login data match with their account data in database.
###### URL
```
https://cinemakuy.herokuapp.com/account/login
```
###### Request Body
```
	{
	"username":"<username>",
	"pass" : "<password>"
	}
```
###### Response
```
{"token":"<random combination of number and letters it works like session>","username":"<the account username, if login success>","respon":"<Login Success if login data is match with data in database and Login Failed, username doesn't exist if the login data is not match>","isSuccess":<true if success and false if not success>}
```

#### Login User Account [POST]
This API can get list of ticket from user.
###### URL
```
https://cinemakuy.herokuapp.com/account/ticket
```
###### Request Body
```
{
	"username":"<username>",
	"token" : "<token>"
}
```
###### Response
```
{
    "respon": "<Message of result>",
    "isSuccess": <Boolean is process success>,
    "data": {
        "<Key>": {
            "chair": "<Chair code>",
            "cinema": {
                "address": "<Cinema Address>",
                "image": "<URL image cinema provider>",
                "location": "<Cinema mall name>",
                "name": "<Cinema provider name>"
            },
            "date": "<date this ticket ex:2019-11-21>",
            "price": <int price ticket>,
            "studio": "<Studio name>",
            "ticcode": "<booking code/uniqcode>",
            "uid": "<username owner ticket>"
        },
		...
    }
}
```


#### Update User Account [POST]
Used to update data of user account ( name, email, password, phone number).
###### URL
```
https://cinemakuy.herokuapp.com/account/update
```
###### Request Body
```
	{
	"name":"<Display Name>",
	"pass" : "<Password>",
	"email":"<User E-mail>",
	"phone":"<Phone Number>",
	"username":"<Username>",
	}
```
###### Response
```
{
    "respon": <"User Updated successfully" if the update success and "User Update Error">,
    "isSuccess": <true if update success and false if update not success>
}
```

#### Change Password User [POST]
Used to change user account password.
###### URL
```
https://cinemakuy.herokuapp.com/account/updatepass
```
###### Request Body
```
{
	"username":"<username of user account>",
	"oldpass":"<current password that will get change>",
	"newpass":"<user account new password>",
	"token":"<current token>"
}
```
###### Response
```
{
	"respon": <"Password Changed successfully" if the update success and "Password Changing Error">,
    "isSuccess": <true if update success and false if update not success>
}
```


#### Logout Account [POST]
Used to delete token in firebase to recognize that the account is already loged out or not loged in.
###### URL
```
https://cinemakuy.herokuapp.com/account/logout
```
###### Response
```
if failed :
{ 	respon : "Failed",isSuccess : false}
if success : 
{ 	respon : "Logout Success",isSuccess : true}
```





### Payment
version payment '/paymeny' is focus on credit payment on the user

#### Make a session payment via Virtual Account [POST]

###### URL
```
https://cinemakuy.herokuapp.com/payment/va
```
###### Request Body
```
{
	price : <int credit that user desire>,
	channel : "va",
	username : "<username>",
	phone : "<phone number>",
	email : "<user email>"	
}
```
###### Response
```
{
	id: <int transaction id>,
	channel: "<channel>",
	kode_pembayaran: "<reff code to pay>",
}
```


#### Make a session payment via Indomaret/Alfamaret [POST]

###### URL
```
https://cinemakuy.herokuapp.com/payment/cstore
```
###### Request Body
```
{ 
	price : 5000,
	channel : "<via (ex:indomaret/alfamaret)>",
	username : "<username>",
	phone : "<phone number>",
	email : "<email>"	
}
```
###### Response
```
{
	id: <int transaction id>,
	channel: "<channel>",
	kode_pembayaran: "<reff code to pay>",
}
```





### Movies
version movies '/movies' is focus on movies part of this API, in this part you can get movies that showing today, cinema that show that movies, seats availability, and you could also order seat in the cinema.


#### List of Indonesian Cities [GET]
Get list of city in indonesia that already integration with us.
###### URL
```
https://cinemakuy.herokuapp.com/movies/cities
```
###### Response
```
[
	"<city1>",
	"<city2>",
	"<city3>",
	...
]
```

#### Now Playing Movies [GET]
Get all movies that now playing on a city.
 default city is "Jakarta"
###### URL
```
https://cinemakuy.herokuapp.com/movies/now_playing?city=<(optional) <city name>>
```
###### Response
```
{
    data: [{
			event_id:<ID of movie>,
			name: "<Movie name>",
			image: "<URL image cover movie>",
			pre_order: <Boolean is preOrder (false if already showing)>,
			rating: "<Age rate of this movie>",
			genre: "<Movie genre>",
			director: "<Name of director>",
			actor: "<List of main actor (actor1, actor2, actor3, etc)>",
			duration: "<Movie duration>",
			synopsis: "<Description synopsis>",
			recommended: <Boolean is recommended>,
			tickets_sold: "<Information count of tickets that already sold>",
			IMDb_rating: "<movie rating based on IMDB>",
			parent_id: <ID of previous sequel movie>
		},
		...
	],
	status : <int status>,
	message : "<Message request Error or Success>"
}
```

#### Schedule Movies on Cinema [GET]
Get schedule movie on all cinema on a city.
 default city is "Jakarta"
 default date is today 
###### URL
```
https://cinemakuy.herokuapp.com/movies/schedule?event_id=<ID of movie>&city=<(optional) city name>&date=<(optional) date>
```
###### Response
```
{
    data: {
        showdates: [
            {
                showdate: "<date yyyy-mm-dd>",
                day: "<day (ex: Today, Mon, Sun, etc)>",
                date: "<date dd (ex: 25, 27, 31, etc)>",
                available: <Boolean availability this date>
            },
			... //(other date that show this film)
        ],
		"cinemas": [
            {
                name: "<Place of cinema>",
                provider_name: "<name of cinema>",
                event_id: <ID of movie>,
                provider_image: "<URL image cinema>",
                address: "<Cinema address>",
                city: "<city name>",
                schedules_list": [
                    {
                        schedule_class: "<Cinema class>",
                        showdate: "2019-08-25",
                        schedules: [
                            {
                                schedule_id: <ID of schedule>,
                                showtime: "<show time (ex: 21.45)>",
                                price: <int price of cinema seat>,
                                seat_available: <int seat availability (0 for available , 1 for unavailable)>
                            },
							... // (other showtime that show this film)
                        ]
                    },
					... // (other class cinema that show this film)
                ]
            },
			... // (other cinemas that show this film)
		]
    },
	status : <int status>,
	message : "<Message request Error or Success>"
}
```

#### Seat on Cinema [GET]
Getting studio data on a cinema studio.
###### URL
```
https://cinemakuy.herokuapp.com/movies/schedule/<ID of schedule>/seats
```
###### Response
```
{
    data: {
        provider: "<name of cinema>",
        max_booked_seats: <int maximal order seat>,
        audi: "<studio name>",
        width: <int total width of studio>,
        height: <int total height of studio>,
        schedule_class: "REGULAR 2D",
        schedule_id: 52431115,
        seat_layout: [
			{
                section_id: "<ID section>",
                section_name: "<name of section>",
                width: <int width of section studio>,
                height: <int height of section studio>, 
                price: <int price of seat in this section>,
                seats: [ 			// array count = width * height
					{
                        code: "<seat number>",
                        row: <int row at (start from 0)>,
                        column: <int column at (start from 0)>,
                        flag: "<flag group>",
                        price: <int price of this seat>,
                        status: <int seat status (-1 : not a seat , 0 : available , 1 : ordered)>,
                        ticket_id: <int ID ticket for this seat>,
                        coordinate: "",
                        seat_in_group: <Boolean is grouping> // if group=true , user have to buy all seats with same flag
                    },
					... (other seat data)
				]
			},
			... (other section)
		],
		provider_image: "<URL image cinema>"
	},
	status : <int status>,
	message : "<Message request Error or Success>"
}
```
#### Sample Image
![Sample ilustration image](seats.png)



#### Buy Seats / Buy Ticket [POST]
Buy ticket and update to database
###### URL
```
https://cinemakuy.herokuapp.com/movies/buyTicket
```
###### Request Body
```
{
	username : "<Username>",
	token : "<User Token>",
	total : <int price of chair>,
	schedule_id : <int id of schedule>,
	orders : [ <Chair object from api above> ],
	provider : {image:"<provider image url>" , location:"Location of cinema" , name : "<name of provider>" , address :"<adress of cinema>"},
	date : "<date of thic ticket>",
	audi : "<audio name>",
	
}
```
###### Response Body
```
{
	respon : "<message>",
	isSuccess : <Boolean is buy success?>
}
```


#### Ordering seats / Checkout seats [POST]
Order seat make a checkout bill to buy that seat while order system will book seat for approximately 7 minutes to make the other can't buy your picked seat.
###### URL
```
https://cinemakuy.herokuapp.com/movies/order
```
###### Request Body
```
{
    seats: [
        {
			code: "<seat number>",
			row: <int row at (start from 0)>,
			column: <int column at (start from 0)>,
			flag: "<flag group>",
			price: <int price of this seat>,
			status: <int seat status (-1 : not a seat , 0 : available , 1 : ordered)>,
			ticket_id: <int ID ticket for this seat>,
			coordinate: "",
			seat_in_group: <Boolean is grouping> // if group=true , user have to buy all seats with same flag
		},
		... (other seat object)
    ],
    audi: "<studio name>",
    event_id: <ID of movie>,
    provider: "<name of cinema>"
}
```
###### Response
```
{
    data: {
        order_id: <ID order>,
        booking_reference: "<reff code booking>",
        event_id: <ID of movie>,
        total_price: "<price ticket>",
        timeout_period: <int time to pay the bill(in sec)>,
        payment_component: [
            {
                payment_title: "<payment name (ex: ticket , conficient fee)>",
                payment_description: "<price>"
            },
            ... // (other payment component)
        ],
        allowed_payment: [
            {
                payment_provider_name: "<payment name (ex: GoPay)>",
                payment_provider_type: <int paymen type>,
                charge_type: "<payment type (ex: percent,amount)>",
                payment_fee: "<value charge type>",
                payment_total: "<value payment total>"
            },
			... // (other allowed payment)
        ],
        tickets": [
            {
                ticket_id: <int ID ticket>,
                name: "<studio class>",
                quantity: <int quantity ticket>,
                price: "<price ticket>",
                seats: "<seat code>"
            },
			... (other ticket with diff studio class)
        ],
        name: "<movie name>",
        image: "<URL movie cover image>",
        showdate: "<date yyy-mm-dd>",
        showtime: "<show time hh:mm>",
        seats: "<seat code (ex: A6;A5;A4)>",
        cinema_name: "<Cinema name (ex:CGV - Grage City Mall)>",
        region: "<city name>"
    },
	status : <int status>,
	message : "<Message request Error or Success>"
}
```

#### Purchase seats / Buy seats [POST]
Buy seat to make the seat belongs to you, In this session user have to choose payment method that user want and pay before session time is up.
###### URL
```
https://cinemakuy.herokuapp.com/movies/pay
```
###### Request Body
```
{
    order_id: <ID order>,
    payment_provider_type: <int payment type>,
	username : "<name user>",
	email : "<email user>",
	total_price : "<total price>"
}
```
###### Response
```
{
    data: {
        order_id: <ID order>,
        purchase_status: <int process status (100 : in process , 101 : transaction failed , 102 : transaction success)>,
        share_message: "<message transaction created>",
        payment_type: "<name of payment (ex:gopay)>",
        payment_channel: {
            virtual_account: {// will be null if not using va
				bank:"<bank name>",
				va_number : "<number of vitual account>"
			},
            gopay: { // will be null if not using gopay
                qr_code: "https://api.veritrans.co.id/v2/gopay/9fa3493a-b743-4704-ae3e-8fe54739830c/qr-code",
                deeplink_url: "gojek://gopay/merchanttransfer?tref=LZ7YX3MAAWKLTZRODCPJIFEM&amount=43000&activity=GP:RR"
            },
            echannel: null
        }
    },
	status : <int status>,
	message : "<Message request Error or Success>"
}
```

#### Check Purchase Status [GET]
Checking purchase status from a order
###### URL
```
https://cinemakuy.herokuapp.com/movies/pay/<ID order>
```
###### Response
```
{
    data: {
        order_id": 12264659,
        purchase_status": <int process status (100 : in process , 101 : transaction failed , 102 : transaction success)>,
        share_message": "<msg status purchase (ex: in_process , purchase_failed)>",
        fnb_purchase_status": 0,
        fnb_share_message": "",
        provider": "<cinema name>"
    },
	status : <int status>,
	message : "<Message request Error or Success>"
}
```
