//Router
const account = require( './routes/account.js');
const movies = require('./routes/movies.js');
const ipaymu = require('./routes/ipaymu.js');
const wumpus = require('./routes/wumpus.js');
const his = require('./routes/historyticket.js');


// Initialize express and body parser
const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())
app.use(express.static('public'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const fs = require('fs');
const mailjet = require ('node-mailjet').connect('74ccbda9a0988e36219873fa797543af', '57e1c3170193b695f1fb75a56f23cb42')


app.use(ipaymu)
app.use(account)
app.use(movies)
app.use(wumpus)





const port = process.env.PORT || 3000;

/// SAMPLE TEST
//API function

app.get('/' , (req , res) => {
	/*
	let file = fs.readFileSync("./ReceiptCinemakuy.html", "utf8");
	file = file.replace("itemid0001" , "http://drive.google.com/uc?export=view&id=1a1DGCBSCAQoWVEaZQGs6XYY9XzKv7dPU")
	file = file.replace("itemid0002" , "Firdiansyah")
	file = file.replace("itemid0003" , "https://media.edutopia.org/styles/responsive_300px_original/s3/masters/d7_images/content/1b/miller-qrcode.jpg")
	file = file.replace("itemid0004" , "Rp. "+numberWithCommas(102000)+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , "123123")
	*/
	//console.log(file);
	res.send("Welcome to Cinemakuy API!");
	//res.download('./', 'f091570b6871a7b9e9e2a81a139d8ad6.txt', function(err) {
    //var file = __dirname + '/upload-folder/dramaticpenguin.MOV';
	//res.download('./f091570b6871a7b9e9e2a81a139d8ad6.txt'); // Set disposition and send it.
});

/*
app.get('/emails' , (req , res) => {
	
	let file = fs.readFileSync("./ReceiptCinemakuy.html", "utf8");
	file = file.replace("itemid0001" , "http://drive.google.com/uc?export=view&id=1a1DGCBSCAQoWVEaZQGs6XYY9XzKv7dPU")
	file = file.replace("itemid0002" , "Firdiansyah")
	file = file.replace("itemid0003" , "https://media.edutopia.org/styles/responsive_300px_original/s3/masters/d7_images/content/1b/miller-qrcode.jpg")
	file = file.replace("itemid0004" , "Rp. "+numberWithCommas(102000)+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , "123123")
	
	const request = mailjet
		.post("send", {'version': 'v3.1'})
		.request({
		  "Messages":[
			{
			  "From": {
				"Email": "cinema.kuyorder@gmail.com",
				"Name": "CinemaKuy"
			  },
			  "To": [
				{
				  "Email": "firdi.ansyah20@gmail.com",
				  "Name": "firdi"
				}
			  ],
			  "Subject": "Greetings from Mailjet.",
			  "TextPart": "My first Mailjet email",
			  "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
			  "CustomID": "AppGettingStartedTest"
			}
		  ]
		})
		request
		  .then((result) => {
			console.log(result.body)
			res.send(result.body);
		  })
		  .catch((err) => {
			console.log(err.statusCode)
			res.send(err.statusCode);
		  })
	
	
})*/

/*


//Api get firebase data
app.get('/test/get' , (req , res) => {
			let ref = database.ref("TEST/"+req.body.v1);
			
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						
						res.send(snapshot.val());
					}else {
						res.send("'Error : Cant find data from key '"+req.body.v1+"'");

					}
			});
})

//Api ADD
app.post('/test/add' , (req , res) => {
			let ref = database.ref("TEST/"+req.body.v1);
			let responBack = { 	respon : "Failed",isSuccess : false};
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						responBack.respon = "Username Already Exist";
					}else {
						ref.set(req.body.v2);
						responBack.isSuccess = true;
						responBack.respon = "User created successfully";

					}
					res.send(responBack);
					responBack = null;
			});
})
//Api DEL
app.post('/test/del' , (req , res) => {
			let ref = database.ref("TEST/"+req.body.v1);
			console.log(req.body);
			let responBack = { 	respon : "Failed",isSuccess : false};
			ref.once('value').then(function(snapshot) {
				
					if(snapshot.val() != null){
						ref.remove();
						responBack.respon = "Delete Success";
						responBack.isSuccess = true;
					}else {
						
						responBack.respon = "Data is not exist";

					}
					res.send(responBack);
					responBack = null;
			});
})

// test
app.get('/test/uidc' , (req,res) => {
	

	let ref = database.ref("uidc");
			
	ref.once('value').then(function(snapshot) {
			if(snapshot.val() != null){
				console.log(snapshot.val());
				res.send({val : snapshot.val()});
			}else {
				res.send("'Error : Cant find data from key 'uidc'");

			}
	}, function(error) {
    // The Promise was rejected.
    console.log(error);
	res.send("'Error : Cant find data from key 'uidc'");
  });

})

app.get('/test/req' , (req,res2) => {
	const options = {
	  hostname: 'https://api.gojekapi.com/v1/web/movie/showtimes?city=Jakarta&date=2019-08-24',
	  port: 80,
	  path: '/upload',
	  method: 'GET'
	};
	
	https.get('https://api.gojekapi.com/v1/web/movie/showtimes?city=Jakarta&date=2019-08-24', (res) => { 
	  console.log('STATUS: ' + res.statusCode);
	  console.log('HEADERS: ' + JSON.stringify(res.headers));

	  // Buffer the body entirely for processing as a whole.
	  var bodyChunks = [];
	  res.on('data', function(chunk) {
		// You can process streamed parts here...
		bodyChunks.push(chunk);
	  }).on('end', function() {
		var body = Buffer.concat(bodyChunks);
		// ...and/or process the entire body here.
		res2.send(JSON.parse(body));
	  })
	});
	
	
})

/// SAMPLE TEST


*/






app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))