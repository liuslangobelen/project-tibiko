const database = require('../config/config.js');

const express = require('express')

const router = express.Router()



const http = require('http');
const https = require('https');
const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;

router.post('/payment/test/:hisID' , (req,res) => {
	
	
	fetch('https://postman-echo.com/post', {
		method: 'post',
		body:JSON.stringify({oke:"oke"}),
		headers: {
			'Content-Type': 'application/json; charset=utf-8'
		 }
	})
	.then(response1 => response1.json())
	.then(json => {
		let ref = database.ref("History/"+req.params.hisID);
		ref.remove();
		res.send("success");
	});
	
});


/*{ // sample
	price : 5000,
	channel : "indomaret"/alfamaret,
	username : "a1",
	phone : "081914830159",
	email : "firdi.ansyah20@gmail.com"	
}*/

// Make a payment session to buy credit in apps
router.post('/payment/cstore' , (req,res) => {
	
	//console.log("test");
	let his = database.ref("History").push();
	let hisID = his.key;
	//console.log("test");
	// first request
	let orderBody = {
						key:"63DFE9D7-155A-4395-BC34-EA2C1862642C",
						pay_method:"cstore",
						product:["saldo"],
						quantity:[1],
						price:[req.body.price],
						unotify:"https://cinemakuy.herokuapp.com/payment/validate/"+req.body.username+"/"+hisID
					};
    console.log(orderBody);
	
	fetch('https://my.ipaymu.com/api/payment/getsid', {
		method: 'post',
		body:    JSON.stringify(orderBody),
		headers: {
			'Content-Type': 'application/json; charset=utf-8'
		 }
	})
	.then(response1 => response1.json())
	.then(json => {
			
			//let db = database.ref("History/"+hisID);
			
			
			console.log(json);
			// Second Request
			let orderBody2 = {
							key:"63DFE9D7-155A-4395-BC34-EA2C1862642C",
							sessionID:json.sessionID,
							name:req.body.username,
							channel:req.body.channel,
							phone:req.body.phone,
							email:req.body.email,
							active:24
						};
			//console.log(orderBody2);
			//res2.send(json);
			
			fetch('https://my.ipaymu.com/api/payment/cstore', {
				method: 'post',
				body:    JSON.stringify(orderBody2),
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				 }
			})
			.then(response2 => response2.json())
			.then(json2 => {
				//console.log(json2);
				// update data in firebase
				let addTransDatabase = {
										tcode : 0,
										tname : "Pembelian Saldo",
										tstatus : -1,
										sessionID : json2.sessionID,
										count : req.body.price,
										date : getDate(),
										expired : json2.expired,
										via : req.body.channel,
										tid : json2.trx_id,
										ref:json2.kode_pembayaran,
										desc : json2.keterangan,
										uid:req.body.username
										 
									}
				// Status
				//-1 => Sedang diproses
				// 0 => Pending
				// 1 => Berhasil
				// 2 => Batal
				// 3 => Refund
				// 6 => Menunggu Settlement
				his.set( addTransDatabase );
				
				let result = {
					id: json2.trx_id,
					channel: orderBody2.channel,
					expired : json2.expired,
					kode_pembayaran: json2.kode_pembayaran,
					desc : json2.keterangan,
					count: req.body.price
				}
				//result.id = ;
				//result.kode_pembayaran = json2.kode_pembayaran;
				
				res.send(result);

			});
	});
	

			
});




/*{ // sample
	price : 5000,
	channel : "va",
	username : "a1",
	phone : "081914830159",
	email : "firdi.ansyah20@gmail.com"	
}*/
// Payment via virtual account
router.post('/payment/va' , (req,res) => {
	
	
	let his = database.ref("History").push();
	let hisID = his.key;
	
	fetch('https://my.ipaymu.com/api/getbniva', {
		method: 'post',
		body:    JSON.stringify({
							key:"63DFE9D7-155A-4395-BC34-EA2C1862642C",
							uniqid:randomInt(100000,9999999),
							price:req.body.price,
							unotify:"https://cinemakuy.herokuapp.com/payment/validate/"+req.body.username+"/"+hisID
						}),
		headers: {
			'Content-Type': 'application/json; charset=utf-8'
		 }
	})
	.then(response2 => response2.json())
	.then(json2 => {
		//console.log(json2);
		// update data in firebase
		let addTransDatabase = {
									tcode : 0,
									tname : "Pembelian Saldo",
									tstatus : -1,
									count : req.body.price,
									date : getDate(),
									via : req.body.channel,
									tid : json2.id,
									ref:json2.va,
									uid : req.body.username
								}
		his.set( addTransDatabase );
		//console.log("test");

		let result = {
					id: json2.id,
					channel: req.body.channel,
					kode_pembayaran: json2.va,
				}
		res.send(result);

	});
	
});

// Validate Payment
router.post('/payment/validate/:username/:hisID' , (req,res) => {
	let ref = database.ref("History/"+req.params.hisID);
	if(req.body.status == 'berhasil' || req.body.status==1){
		
		let refValidation = database.ref("History/"+req.params.hisID);
		refValidation.once('value').then(function(data) {
			if(data.val().tstatus = 1){
				res.send({isPayed : false});
				return;
			}
				
			let saldo = database.ref("Account/"+req.params.username+"/udata/credit");
			ref.update({tstatus : 1});
			saldo.once('value').then(function(snapshot) {
				console.log( data.val() );
				console.log( snapshot.val() );
				console.log("Payment Success");
				saldo.set(snapshot.val() + data.val().count);
				res.send({isPayed : true});
			});
		});
	}else{
		ref.remove();
		res.send({isPayed : false});
	}
	
});

/* if(req.body.status != 1 && req.body.status != 0){
		let newChildRef = database.ref("History/"+req.params.hisID+"/tstatus");
		newChildRef.set( 0 );
		res.send({isPayed : false});
	}else if(req.body.status == 1){
	
		let refValidation = database.ref("History/"+req.params.hisID);
		refValidation.once('value').then(function(data) {	
			//Validation
			if(data.val().tcode != 0){
				res.send({isPayed : false});
				return;
			}
			if(data.val().tid != req.body.trx_id || data.val().tstatus != 1 ){
				res.send({isPayed : false});
				return;
			}
			
			refValidation.child("tstatus").set( 1 );
			
			let saldo = database.ref("TEST/"+req.params.username+"/saldo");	
			saldo.once('value').then(function(snapshot) {
				console.log("test");
				saldo.set(snapshot.val() + data.val().count);
				res.send({isPayed : true});
			});
		});	
		
	}else{
		res.send({isPayed : false});
	} */

function getHistoryID(callback) {
	let id = database.ref("HistoryCounter");
	id.once('value').then(function(snapshot) {				
			if(snapshot.val() != null){
				let valueCounter = snapshot.val();
				id.set( valueCounter + 1 );
				callback("his"+valueCounter)
			}else{
				callback(null);
			}
			
	});	
	
}


function getDate() {

    var date = new Date();

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day;

}
function randomInt(min , max){
	return min + Math.floor(Math.random() * Math.floor((max-min)));
}


module.exports = router;