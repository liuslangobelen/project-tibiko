
const database = require('../config/config.js');

const express = require('express')

const router = express.Router()

//login
/*
{
	username : "name of user",
	token : "",
	pass : ""
}
*/
router.post('/wumpus/login' , (req , res) => {

			let ref = database.ref("wumpus/account/"+req.body.username);
			ref.once('value').then(function(snapshot) {
				if(snapshot.val() != null){
					if(snapshot.val().token!=null && snapshot.val().token!="" && snapshot.val().token == req.body.token){
						//Login Success token
						
						res.send({message : "Kamu berhasil masuk dengan token" , data : snapshot.val()  , isSuccess : true});
					}else if(snapshot.val().pass!=null && snapshot.val().pass !="" && snapshot.val().pass == req.body.pass){
						//Login Success pass
						
						let newToken = token();
						ref.update({token: newToken });
						let dataWillbeSend = snapshot.val();
						dataWillbeSend.token = newToken;
						res.send({message : "Kamu berhasil masuk"  , data : dataWillbeSend, isSuccess : true});
					}else if(snapshot.val().token!=null && snapshot.val().token !="" && req.body.token != "" && snapshot.val().token != req.body.token){
						//Login Failed
						res.send({message : "Gagal masuk, token tidak cocok" , data:null  , isSuccess : false});
					}else{
						//Login Failed
						res.send({message : "Gagal masuk, password tidak cocok" , data:null , isSuccess : false});
					}
				}else{
					//Login Failed
					res.send({message : "Gagal masuk, username tidak ditemukan" , data:null  , isSuccess : false});
				}
			});
				
})

/*
{
	username : "name of user",
	pass : "",
	email : ""
}
*/
router.post('/wumpus/register' , (req , res) => {
			
			if(req.body.username==null||req.body.username == ""){
				res.send({message : "Gagal daftar, username kosong"  , isSuccess : false});
				return;
			}else if(req.body.email==null||req.body.email == ""){
				res.send({message : "Gagal daftar, email kosong" , isSuccess : false});
				return;
			}else if(req.body.pass == null||req.body.pass == ""){
				res.send({message : "Gagal daftar, password kosong" , isSuccess : false});
				return;
			}

			let ref = database.ref("wumpus/account/"+req.body.username);
			ref.once('value').then(function(snapshot) {
				if(snapshot.val() != null){
					res.send({message : "Gagal daftar, user telah ada tolong coba username lain"  , isSuccess : false});
				}else{
					let newUser = 	{
									  email : req.body.email,
									  game : {
										gameRoom : "",
										state : "lobby",
										tim : ""
									  },
									  pass : req.body.pass,
									  token : token(),
									  username : req.body.username
									}
					//Register success
					ref.set(newUser);
					res.send({message : "Pendafataran berhasil"  , isSuccess : true});
									
				}
			});
				
})

/*
{
	username : "name of user"
}
*/
router.get('/wumpus/state/:username' , (req , res) => {
			
			if(req.params.username==null||req.params.username == ""){
				res.send({message : "user tidak dapat ditemukan"  , isSuccess : false});
				return;
			}

			let ref = database.ref("wumpus/account/"+req.params.username);
			ref.once('value').then(function(snapshot) {
				if(snapshot.val() != null){
					res.send({data : snapshot.val().game , isSuccess : true});
				}else{
					res.send({data:null , isSuccess : false});
				}
			});
				
})


/*
{
	username : "name of user",
	token : "",
	data : null
}
*/
router.post('/wumpus/save/state' , (req , res) => {
			
			if(req.body.username==null||req.body.username == ""){
				res.send({message : "Gagal menyimpan, username kosong"  , isSuccess : false});
				return;
			}else if(req.body.token==null||req.body.token == ""){
				res.send({message : "kamu perlu menggunakan token untuk menyimpan" , isSuccess : false});
				return;
			}

			let ref = database.ref("wumpus/account/"+req.body.username);
			ref.once('value').then(function(snapshot) {
				if(snapshot.val() != null){
					if(snapshot.val().token!=null && snapshot.val().token!="" && snapshot.val().token == req.body.token){
						let game = {
									gameRoom : req.body.data.currentGameRoom,
									state : req.body.data.currentState,
									tim : req.body.data.currentTim
								}
						ref.child('game').update(game);
						res.send({message : "Berhasil menyimpan state"  , isSuccess : true});
					}else{
						
						res.send({message : "Gagal menyimpan token invalid"  , isSuccess : false});
					}
				}else{
					
					res.send({message : "Gagal meyimpan user tidak ditemukan" , isSuccess : false});
				}
			});
				
})

var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var token = function() {
    return rand() + rand(); // to make it longer
};

module.exports = router;