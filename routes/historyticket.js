const database = require('../config/config.js');

const express = require('express')

const router = express.Router()



// test api to geet id account
router.get('/history/get/:id' , (req,res) => {
	let ref = database.ref("History");
	ref.orderByChild('uid').equalTo(req.params.id).once('value').then(function(snapshot) {
			//console.log(snapshot.val());
			res.send(snapshot.val());
	});
});


// test api to geet id account
router.get('/ticket/get/:id' , (req,res) => {
	let ref = database.ref("Ticket");
	ref.orderByChild('uid').equalTo(req.params.id).once('value').then(function(snapshot) {
			//console.log(snapshot.val());
			res.send(snapshot.val());
	});
});


module.exports = router;