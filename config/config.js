// Initialize Firebase
const admin = require("firebase");

//let serviceAccount = require("path/to/tibiko-backend-firebase-adminsdk-8p2h8-e6fa855538.json");
let config = {
  "apiKey": "AIzaSyDooL6KoZlhkBRUD2FyKJf3ItWxAoZhawA",
  "authDomain": "tibiko-backend.firebaseapp.com",
  "databaseURL": "https://tibiko-backend.firebaseio.com",
  "projectId": "tibiko-backend",
  "storageBucket": "tibiko-backend.appspot.com",
  "messagingSenderId": "996168681793",
  "appId": "1:996168681793:web:3cefa62ef9a80e82"
};

//config.credential = admin.credential.cert(serviceAccount);
//{
//  credential: admin.credential.cert(serviceAccount),
//  databaseURL: "https://tibiko-backend.firebaseio.com"
//}
admin.initializeApp(config);
const database = admin.database();

module.exports = database;